import java.time.LocalDate;
import java.time.Period;

public class Person {

    public enum Genero {
        MASC, FEM
    }

    String nome;
    LocalDate nasc;
    int idade;
    Genero genero;
    String email;
    
    public String getNome() {
    	return nome;
    }
    
    /*public int getIdade() {
    	LocalDate hoje = LocalDate.now();
    	if(genero != null && hoje != null) {
    		return Period.between(nasc, hoje).getYears();
    	}
		return 0;
    }*/
    
    public int getIdade() {
    	return idade;
    }
    
    public String getEmail() {
    	return email;
    }
    
    public Genero getGenero() {
    	return genero;
    }
}