import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

import java.util.Deque;
import java.util.Iterator;
import java.util.ArrayDeque;

public class App {
	
	public static void main(String[] args) {
		/* [EXERCICIO 1]
		 * Descreva as vantagens no uso de exce��es, mostrando exemplos de sua aplica��o.
		 * E porque quando trabalhamos com arquivos, esse tratamento � obrigat�rio?
		 * 
		 * Qualquer opera��o que envolva arquivos pode falhar. 
		 * Portanto a �nica forma de tratar situa��es de erro com arquivos � tratamento de excess�es.
		 * Derivadas da IOException, s�o excess�es verificadas. 
		 * Por esse motivo � obrigat�rio capturar e tratar essas excess�es. 
		 * (ou delega-las para serem tratadas, atrav�s de throws)
		 * 
		 */
		
		
		/* [EXERCICIO 2]
		 * Imagine um arquivo em formato texto, onde cada registro cont�m um nome e uma certa
		 * quantidade de notas (desconhecida):
		 * 
		 * Escreva um programa capaz de ler os dados do arquivo,
		 * exibindo na tela somente os alunos cuja m�dia dos
		 * trabalhos seja superior ou igual a 7.
		 */
		System.out.println("Exerc�cio 2: ");
		Path filePath = Paths.get("dados.txt");
		try (BufferedReader br = Files.newBufferedReader(filePath, Charset.defaultCharset())) {
			String lin = null;
			while((lin = br.readLine()) != null ) {
				Scanner sc = new Scanner (lin);
				String nome = null; double media = 0; int count = 0;
				if(sc.hasNext()) nome = sc.next();
				while(sc.hasNext()) {
					String valor = sc.next();
					try {
						media += Double.parseDouble(valor);
						count++;
					} catch (NumberFormatException e) {
						System.err.println("Tentativa de ler nota falhou: "  + e.getMessage());
					}
				}
				sc.close(); // fecha stream
				media /= count;
				if(media >= 7.0)
					System.out.format("Aluno %s obteve m�dia %2.1f.%n", nome, media); // %2 = 2 caracteres antes da virgula, at� %.2.1f = 2 antes e 1 depois
			}
		} catch (IOException e) {
			System.out.println("Houve um erro na leitura de arquivo: " + e);
		}
		
		/* [EXERCICIO 3]
		 * Suponha uma cole��o lista de objetos Pessoa, onde cada uma tem a seguinte estrutura: (Classe Person)
		 * Usando o recurso de opera��es de agrega��o, que aceitam express�es lambda como par�metro, escreva um trecho de c�digo para realizar cada uma das a��es abaixo:
			1. Exibir o nome das pessoas maiores de 18 anos
			2. Exibir o email das mulheres maiores de 60 anos
			3. Exibir a m�dia de idade dos homens
		 */
		System.out.println("\nExerc�cio 3: "); //exemplos
		ArrayList<Person> pessoas = new ArrayList<>();
		Person a = new Person();
		a.nome = "AAA";
		a.idade = 17;
		a.email = "A@";
		a.genero = Person.Genero.FEM;
		pessoas.add(a);
		
		Person b = new Person();
		b.nome = "BBB";
		b.idade = 18;
		b.email = "B@";
		b.genero = Person.Genero.MASC;
		pessoas.add(b);
		
		Person c = new Person();
		c.nome = "CCC";
		c.idade = 60;
		c.email = "C@";
		c.genero = Person.Genero.FEM;
		pessoas.add(c);
		
		Person d = new Person();
		d.nome = "DDD";
		d.idade = 30;
		d.email = "D@";
		d.genero = Person.Genero.FEM;
		pessoas.add(d);	
		
		Person e = new Person();
		e.nome = "EEE";
		e.idade = 71;
		e.email = "E@";
		e.genero = Person.Genero.FEM;
		pessoas.add(e);
		
		Person f = new Person();
		f.nome = "FFF";
		f.idade = 36;
		f.email = "F@";
		f.genero = Person.Genero.MASC;
		pessoas.add(f);
		
		Person g = new Person();
		g.nome = "FFF";
		g.idade = 50;
		g.email = "F@";
		g.genero = Person.Genero.MASC;
		pessoas.add(g);
		
		System.out.println("1. Exibir o nome das pessoas maiores de 18 anos");
		pessoas.stream().filter(p -> p.getIdade() >= 18).forEach(p -> System.out.format("%s, idade: %s%n", p.getNome(), p.getIdade()));
		
		System.out.println("\n2. Exibir o email das mulheres maiores de 60 anos");
		pessoas.stream().filter(p -> p.getIdade() >= 60 && p.getGenero() == Person.Genero.FEM).forEach(p -> System.out.format("%s%n", p.getEmail()));
		
		System.out.println("\n3. Exibir a m�dia de idade dos homens");
		double media = pessoas.stream().filter(p -> p.getGenero() == Person.Genero.MASC).mapToInt(Person::getIdade).average().getAsDouble();
		System.out.println(media);
		
		
		/* [Quest�o 4]
		 * Em uma estrutura do tipo Deque<Integer> est�o armazenados n�meros inteiros positivos e negativos.
		 * Escreva um m�todo que recebe uma estrutura desse tipo por par�metro e retorne a mesma estrutura,
		 * com os mesmos elementos na mesma ordem, por�m todos com o sinal trocado. 
		 * Se necess�rio, use alguma estrutura auxiliar (apenas UMA).
		 * 
		 */
		System.out.println("\nExerc�cio 4: ");
		Deque<Integer> deque = new ArrayDeque<Integer>();
		deque.add(4);deque.add(-3);deque.add(-2);deque.add(1);deque.add(0);
		System.out.println("Original:     " + deque);
		System.out.println("por For each: " + trocaSinal(deque));// ver fun��o static
		System.out.println("por Stream:   " + trocaSinalStream(deque)); // ver fun��o static
		System.out.println("Por iterator: " + trocaSinalIterator(deque)); // ver fun��o static
		System.out.println("Por Stream(P) " + trocaSinalStreamNoProprio(deque));// troca usando stream mudando o proprio deque
		System.out.println("Por Stream(P) " + trocaSinalStreamNoProprio(deque));// troca usando stream mudando o proprio deque
		System.out.println("Por Stream(P) " + trocaSinalStreamNoProprio(deque));// troca usando stream mudando o proprio deque
		
	
		
		
		/* [Quest�o 5]
		 * Considere as defini��es das classes Funcionario e Cadastro.
		 * Escreva o c�digo do m�todo exibeTotalPorCargo, que deve mostrar, para cada cargo diferente na lista,
		 * o seu nome e o total de funcion�rios no cargo. Explore obrigatoriamente o uso de conjuntos ou dicion�rios.
		 */
		System.out.println("\nExerc�cio 5: ");
		Cadastro funcionarios = new Cadastro();
		funcionarios.add(new Funcionario("A", "Pedreiro"));
		funcionarios.add(new Funcionario("B", "Marceneiro"));
		funcionarios.add(new Funcionario("C", "Pedreiro"));
		funcionarios.add(new Funcionario("D", "Engenheiro"));
		funcionarios.add(new Funcionario("E", "Pedreiro"));
		funcionarios.add(new Funcionario("F", "Engenheiro"));
		funcionarios.exibeTotalPorCargo();
		
	}
	
	public static Deque<Integer> trocaSinal(Deque<Integer> numeros) {
		Deque<Integer> trocado = new ArrayDeque<>(); // ou = numeros.getClass().newInstance(); // seria mais correto para instanciar do mesmo objeto, mas vai dar throw em InstantiationException e IllegalAccessException
		for(Integer i : numeros) {
			trocado.add(i*-1);
		}
		return trocado; // aqui estou fazendo um c�pia dela basicamente, agora ficou a duvida, tem que ser a mudan�a na mesma estrutura mesmo? Ou com mesma estrutura seria mesma estrutura de dados (Deque, Array, etc)?
	}	
	
	
	public static Deque<Integer> trocaSinalIterator(Deque<Integer> numeros) {
		Deque<Integer> trocado = new ArrayDeque<>();
		Iterator<Integer> it = numeros.iterator();
		while(it.hasNext()) {
			trocado.add(it.next()*-1);
		}
		return trocado;
	}
	
	public static Deque<Integer> trocaSinalStream(Deque<Integer> numeros) {
		Deque<Integer> trocado = new ArrayDeque<>();
		numeros.stream().forEach(n -> trocado.addLast(n*-1));
		return trocado;
	}
	
	public static Deque<Integer> trocaSinalStreamNoProprio(Deque<Integer> numeros) {
		Deque<Integer> trocado = new ArrayDeque<>(); // ou = numeros.getClass().newInstance(); // seria mais correto para instanciar do mesmo objeto, mas vai dar throw em InstantiationException e IllegalAccessException
		numeros.stream().forEach(n -> trocado.add(n));
		numeros.clear();
		trocado.stream().forEach(n -> numeros.add(n*-1));
		return numeros;
	}
}