import java.util.Map;
import java.util.Map.Entry;
import java.util.ArrayList;
import java.util.HashMap;

public class Cadastro{
	private ArrayList<Funcionario> funcionarios;
		
	public Cadastro() {
		funcionarios = new ArrayList<>();
	}
	
	public void add(Funcionario func) {
    	funcionarios.add(func);
    }
    public void exibeTotalPorCargo() {
    	Map<String, Integer> map = new HashMap<>();
    	for(Funcionario f : funcionarios) {
    		if(map.containsKey(f.getCargo())) {
    			Integer i = map.get(f.getCargo()) + 1;
    			map.put(f.getCargo(), i);
    		} else {
    			map.put(f.getCargo(), 1);
    		}
    	}
    	map.entrySet().stream().forEach(entry -> System.out.format("%s: %s funcionários.%n",entry.getKey(), entry.getValue()));
    }

    
}
